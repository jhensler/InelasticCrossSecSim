# Sicma
### The Simulation of Inelastic Cross-Section Measurements using ALPIDE telescope 

https://gitlab.example.com/<namespace>/<project>/badges/<branch>/pipeline.svg

## Dependencies
* [GEANT4](https://geant4-userdoc.web.cern.ch/UsersGuides/InstallationGuide/html/installguide.html) (required for simulation)

## Compilation

The CMake build system is used for compilation. To compile, run the following commands

**Unix** 
```
$ mkdir build && cd build/
$ cmake ..
$ make
```

**Windows / Qt**
```
$ mkdir build && cd build/
$ cmake ..
$ cmake --build . --config Release
```

## Running 

To run the simulation in visual mode, execute without any additional flags otherwise add a path to a run macro file. The run macro will can take care of simulating runs with specific geometry and beam setting, then changes and takes runs with the changed geometry again. This is very usefull for making parameter scans or in general running the simulation. For debuggung the visual mode is recommended.

**Unix** 
```
$ cd build/Release
$ ./sim <run_macro.mac>
```
**Windows / Qt** 
```
$ cd build/Release
$ sim <run_macro.mac>
```

## Get started

Before the simulation can run an output directory needs to be created in the project folde, where all rootfiles can be stored. This can be done by
```
$ mkdir output
```
