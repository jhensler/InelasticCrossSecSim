#include <iostream>
#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4VisManager.hh"
#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"
#include "G4SystemOfUnits.hh"
#include "FTFP_BERT.hh"
#include "QBBC.hh"

#include "RunManager.hh"
#include "construction.hh"
#include "physics.hh"
#include "action.hh"

int main(int argc, char **argv)
{
    RunManager *runManager = new RunManager();

    // Construction
    runManager->SetUserInitialization(new SimulationConstruction());

    // Physics
    G4VModularPhysicsList *physicsList = new QBBC;
    // runManager->SetUserInitialization(new PhysicsList()); // own physics list
    runManager->SetUserInitialization(physicsList); // Geant4 physics list

    // Action
    ActionInitialization *Action = new ActionInitialization();
    runManager->SetUserInitialization(Action);

    runManager->Initialize();

    // Only create ui visulization if no argument given (run macro)
    G4UIExecutive *ui = 0;
    if (argc == 1)
    {
        ui = new G4UIExecutive(argc, argv);
    }

    G4VisManager *visManager = new G4VisExecutive();
    visManager->Initialize();

    G4UImanager *uiManager = G4UImanager::GetUIpointer();
    if (ui)
    {
        // No macro file -> Use UI
        uiManager->ApplyCommand("/control/execute ../vis.mac"); // Executes the visualisation script
        uiManager->ApplyCommand("/run/printProgress -1");       // Removes the "Event xy started print"
        ui->SessionStart();
    }
    else
    {
        // Run with macro file
        uiManager->ApplyCommand("/run/printProgress -1"); // Removes the "Event xy started print"
        G4String command = "/control/execute ";
        G4String fileName = argv[1];
        uiManager->ApplyCommand(command + fileName); // Starts the simulation with a run macro
        G4cout << "Geant4 started with run macro: " << fileName << G4endl;
    }

    return 0;
}
