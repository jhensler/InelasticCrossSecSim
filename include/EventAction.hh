#ifndef EVENTACTION_HH
#define EVENTACTION_HH

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "G4Event.hh"
#include "G4UImanager.hh"
#include "G4AnalysisManager.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4GenericMessenger.hh"
#include "G4SystemOfUnits.hh"
#include "G4ExceptionHandler.hh"

#include "UserHit.hh"
#include "construction.hh"
#include "RunManager.hh"
#include <Pixel.hpp>
#include <Event.hpp>
#include <TTree.h>

class EventAction : public G4UserEventAction
{
public:
    EventAction();
    ~EventAction();

    void BeginOfEventAction(const G4Event *event) override;
    void EndOfEventAction(const G4Event *event) override;

    RunManager *runManager = NULL;
    RunAction *runAction = NULL;

    G4ExceptionHandler *Exception;

    // Timing (for exporting to corry)
    G4double StartEventTime;
    G4double EndEventTime;

    // Monte Carlo Variables
    G4bool bIsEntered = false;
    G4bool bIsExited = false;
    G4bool bIsPassed = false;
    G4bool bIsInelastic = false;
    G4bool bIsElastic = false;

    // Beam properties
    G4double BeamPosDet0X;
    G4double BeamPosDet0Y;
    G4double BeamEnergy;

    // Measurement variables
    G4bool bIsTrigger = false;
    G4bool bIsInTrack = false;
    G4int NrInTrack = 0;
    G4int NrOutTrack = 0;
    G4bool bIsOutSingleTrack = false;
    G4bool bIsOutMultipleTrack = false;
    G4bool bIsNoOutTrack = false;

    // Maps the detector IDs to the hit vectors for classification
    std::map<int, std::vector<UserHit> *> detector_hitvector_map{
        {0, &HitsDet0},
        {1, &HitsDet1},
        {2, &HitsDet2},
        {3, &HitsDet3},
        {4, &HitsDet4},
        {5, &HitsDet5},
    };
    // Store hit data for classification
    std::vector<UserHit> HitsDet0;
    std::vector<UserHit> HitsDet1;
    std::vector<UserHit> HitsDet2;
    std::vector<UserHit> HitsDet3;
    std::vector<UserHit> HitsDet4;
    std::vector<UserHit> HitsDet5;

    G4ThreeVector TargetInTrack;  // Momentum dirction of proton entering the target (used for calculating the scattering angle at the target)
    G4ThreeVector TargetOutTrack; // Momentum dirction of proton leaving the target (used for calculating the scattering angle at the target)
    G4ThreeVector IncomingTrack;  // Momentum direction of proton at ALP0 (used to calculate incoming track angle global)
    G4double EnteringEnergy = 0 * MeV;

    // Messenger
    G4GenericMessenger *fMessenger;
    G4bool bIsRestrictedInTrack;

private:
    double hResX;
    double hResY;

    G4double CalculateMSCAngle(G4double E, int z, G4double X_0, G4double x, G4double m);
    std::pair<G4double, G4double> CalculateTotalScattering(G4ThreeVector TargetIntrack, G4ThreeVector TargetOutTrack);

    void CorryExport();
};

#endif