#ifndef CONSTRUCTION_HH
#define CONSTRUCTION_HH

#include "G4SystemOfUnits.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Sphere.hh"
#include "G4PVPlacement.hh"
#include "G4NistManager.hh"
#include "G4GenericMessenger.hh"
#include "G4SubtractionSolid.hh"
#include <cmath>

class SensitiveDet;

class SimulationConstruction : public G4VUserDetectorConstruction
{
public:
    SimulationConstruction();
    ~SimulationConstruction();

    virtual G4VPhysicalVolume *Construct();

    G4double target_thickness;
    G4bool bIsTargetIn;

    G4double x_pitch = 29.24 * micrometer;
    G4double y_pitch = 26.88 * micrometer;

    G4double n_pixels = 1024;
    G4double m_pixels = 512;

private:
    G4GenericMessenger *fMessenger;

    virtual void ConstructSDandField();
    void DefineMaterials();
    void DefineRotations();

    // Measures ---------------------------------------------------------

    G4double world_halfsizeX = 0.12 * m;
    G4double world_halfsizeY = 0.12 * m;
    G4double world_halfsizeZ = 1 * m;

    G4double target_halfsizeX = 7.5 / 2 * cm;
    G4double target_halfsizeY = 7.5 / 2 * cm;

    G4double ALPIDE_halfsizeX = 1024 * x_pitch / 2;
    G4double ALPIDE_halfsizeY = 512 * y_pitch / 2;
    G4double ALPIDE_halfsizeZ = 50. / 2 * micrometer;

    G4double sci_halfsizeX = 45. / 2 * mm;
    G4double sci_halfsizeY = 25. / 2 * mm;
    G4double sci_halfsizeZ = 10. / 2 * mm;

    G4double outerTholder_halfsizeX = 9.9 / 2 * cm;
    G4double outerTholder_halfsizeY = 9 / 2 * cm;
    G4double outerTholder_halfsizeZ = 1. / 2 * cm;

    G4double innerTholder_halfsizeX = 8 / 2 * cm;
    G4double innerTholder_halfsizeY = 8.1 / 2 * cm;
    G4double innerTholder_halfsizeZ = 0.7 / 2 * cm;

    G4double windowTholder_halfsizeX = 5.6 / 2 * cm;
    G4double windowTholder_halfsizeY = 8 / 2 * cm;
    G4double windowTholder_halfsizeZ = 1.1 / 2 * cm;

    // X-Coordinates from corry alignment need additional - sign as x axis is mirrored in G4 coordinates

    G4double ALP2posX = -1 * 0 * micrometer;
    G4double ALP2posY = 0 * micrometer;
    G4double ALP2posZ = -target_thickness / 2 - 0.7 * cm;
    G4double ALP2rotX = 0 * degree;
    G4double ALP2rotY = 0 * degree;
    G4double ALP2rotZ = 0 * degree;

    G4double ALP1posX = -1 * 0 * micrometer;
    G4double ALP1posY = 0 * micrometer;
    G4double ALP1posZ = ALP2posZ - 21.6 * mm;
    G4double ALP1rotX = 0 * degree;
    G4double ALP1rotY = 0 * degree;
    G4double ALP1rotZ = 0 * degree;

    G4double ALP0posX = -1 * 0 * micrometer;
    G4double ALP0posY = 0 * micrometer;
    G4double ALP0posZ = ALP1posZ - 21.6 * mm;
    G4double ALP0rotX = 0 * degree;
    G4double ALP0rotY = 0 * degree;
    G4double ALP0rotZ = 0 * degree;

    G4double ALP3posX = -1 * 0 * micrometer;
    G4double ALP3posY = 0 * micrometer;
    G4double ALP3posZ = target_thickness / 2 + 0.8 * cm;
    G4double ALP3rotX = 0 * degree;
    G4double ALP3rotY = 0 * degree;
    G4double ALP3rotZ = 0 * degree;

    G4double ALP4posX = -1 * 0 * micrometer;
    G4double ALP4posY = 0 * micrometer;
    G4double ALP4posZ = ALP3posZ + 21.6 * mm;
    G4double ALP4rotX = 0 * degree;
    G4double ALP4rotY = 0 * degree;
    G4double ALP4rotZ = 0 * degree;

    G4double ALP5posX = -1 * 0 * mm;
    G4double ALP5posY = 0 * micrometer;
    G4double ALP5posZ = ALP4posZ + 21.6 * mm;
    G4double ALP5rotX = 0 * degree;
    G4double ALP5rotY = 0 * degree;
    G4double ALP5rotZ = 0 * degree;

    G4ThreeVector ALP0Loc = G4ThreeVector(ALP0posX, ALP0posY, ALP0posZ);
    G4ThreeVector ALP1Loc = G4ThreeVector(ALP1posX, ALP1posY, ALP1posZ);
    G4ThreeVector ALP2Loc = G4ThreeVector(ALP2posX, ALP2posY, ALP2posZ);
    G4ThreeVector ALP3Loc = G4ThreeVector(ALP3posX, ALP3posY, ALP3posZ);
    G4ThreeVector ALP4Loc = G4ThreeVector(ALP4posX, ALP4posY, ALP4posZ);
    G4ThreeVector ALP5Loc = G4ThreeVector(ALP5posX, ALP5posY, ALP5posZ);

    G4RotationMatrix *ALP0Rot;
    G4RotationMatrix *ALP1Rot;
    G4RotationMatrix *ALP2Rot;
    G4RotationMatrix *ALP3Rot;
    G4RotationMatrix *ALP4Rot;
    G4RotationMatrix *ALP5Rot;
    G4RotationMatrix *SciCoverRot;

    G4double sciZpos = ALP0posZ - 6.2 * cm;

    // Solids -------------------------------------------------------------
    G4Box *solidWorld;
    G4Box *solidTarget;
    G4Box *solidDet;
    G4Box *solidDetOBM;
    G4Box *solidSci;
    G4VSolid *solidSciCover;

    G4Box *solidouterTholder;
    G4Box *solidinnerTholder;
    G4Box *solidwindowTholder;
    G4SubtractionSolid *solidpreTholder;
    G4SubtractionSolid *solidTholder;

    // Logics-------------------------------------------------------------
    G4LogicalVolume *logicWorld;
    G4LogicalVolume *logicTarget;
    G4LogicalVolume *logicALPIDE;
    G4LogicalVolume *logicALPIDEOBM;
    G4LogicalVolume *logicSci;
    G4LogicalVolume *logicSciCover;
    G4LogicalVolume *logicTholder;

    // Physics-------------------------------------------------------------
    G4VPhysicalVolume *physWorld;
    G4VPhysicalVolume *physTarget;
    G4VPhysicalVolume *physTholder;
    G4VPhysicalVolume *physSci;
    G4VPhysicalVolume *physSciCover;
    G4VPhysicalVolume *physOBM1;
    G4VPhysicalVolume *physOBM2;

    // Signle ALPIDEs
    G4VPhysicalVolume *physALPIDE0;
    G4VPhysicalVolume *physALPIDE1;
    G4VPhysicalVolume *physALPIDE2;
    G4VPhysicalVolume *physALPIDE3;
    G4VPhysicalVolume *physALPIDE4;
    G4VPhysicalVolume *physALPIDE5;

    // Mats-------------------------------------------------------------
    G4Material *worldMat;
    G4Material *targetMat;
    G4Material *detMat;
    G4Material *sciMat;
    G4Material *TholderMat;
};

#endif
