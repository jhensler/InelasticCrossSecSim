#ifndef RUNACTION_HH
#define RUNACTION_HH

#include "G4UserRunAction.hh"
#include "G4AnalysisManager.hh"
#include "G4RunManager.hh"
#include "G4SystemOfUnits.hh"
#include "G4Run.hh"
#include "G4GenericMessenger.hh"
#include "G4Timer.hh"

#include <Pixel.hpp>
#include <Event.hpp>
#include <TTree.h>
#include <TFile.h>

class RunAction : public G4UserRunAction
{
public:
    RunAction();
    ~RunAction();

    virtual void BeginOfRunAction(const G4Run *);
    virtual void EndOfRunAction(const G4Run *);

    G4GenericMessenger *fMessenger;
    G4String OutputPath;

    G4Timer *TimeStampTimer;

    TFile *CorryFile;

    TTree *event_tree_; // Tree to store events
    TTree *pixel_tree_; // Tree to store pixel

    corryvreckan::Event *event_{};                                // Buffer event
    std::vector<std::vector<corryvreckan::Pixel *> *> pixels_{6}; // Buffer pixel vector
};

#endif