#include "SensitiveDet.hh"

SensitiveDetector::SensitiveDetector(G4String name) : G4VSensitiveDetector(name)
{
    runManager = static_cast<RunManager *>(RunManager::GetRunManager());

    Exception = new G4ExceptionHandler;

    if (runManager && runManager->eventAction)
    {
        eventAction = runManager->eventAction;
    }
    else
    {
        Exception->Notify("EventAction", "No eventAction found", FatalException, "No eventAction could by found by the SensitiveDetector!");
    }

    if (runManager && runManager->runAction)
    {
        runAction = runManager->runAction;
    }
    else
    {
        Exception->Notify("EventAction", "No runAction found", FatalException, "No runAction could by found by the SensitiveDetector!");
    }
}

SensitiveDetector::~SensitiveDetector()
{
}

G4bool SensitiveDetector::ProcessHits(G4Step *aStep, G4TouchableHistory *R0hist)
{
    // Acess the Track corresponding to the Step
    G4Track *track = aStep->GetTrack();

    // Create hit object
    UserHit hit = UserHit();
    hit.SetTrackID(track->GetTrackID());
    hit.SetHitPosition(track->GetPosition());

    runAction->TimeStampTimer->Stop();
    G4double CurrentTime = runAction->TimeStampTimer->GetRealElapsed() * 1e6;
    hit.SetTimeStamp(CurrentTime);

    // Acess the Step Points
    G4StepPoint *preStepPoint = aStep->GetPreStepPoint();
    G4StepPoint *postStepPoint = aStep->GetPostStepPoint();

    // Acess the StepSatus of each StepPoint
    G4StepStatus PreStepStatus = preStepPoint->GetStepStatus();
    G4StepStatus PostStepStatus = postStepPoint->GetStepStatus();

    // Acess the Volumes of the StepPoints
    G4String PostStepVolume = postStepPoint->GetPhysicalVolume()->GetName();
    G4String PreStepVolume = preStepPoint->GetPhysicalVolume()->GetName();

    // Determine the Origin of the detected particle
    G4String ParticleOriginVolume = track->GetOriginTouchable()->GetVolume()->GetName();

    // Get the Copy Nr of the detector hit (to differentiate between the different detectors)
    G4int CopyNo = aStep->GetPreStepPoint()->GetPhysicalVolume()->GetCopyNo();

    // Temporary particle definition and inelastic process (not dynamic by now)
    G4ParticleDefinition *ParticleDefinition = G4Proton::Definition();
    G4String InelasitcProcessName = "protonInelastic";

    // Target
    if (CopyNo == 101)
    {
        // Detect ingoing proton ---------------------------------------------------------------------------------------
        if (PreStepStatus == fGeomBoundary && track->GetParticleDefinition() == ParticleDefinition && ParticleOriginVolume == "physWorld")
        {
            // G4cout << "Particle entered the target" << G4endl;
            eventAction->bIsEntered = true;

            if (eventAction->TargetInTrack == G4ThreeVector(0, 0, 0)) // Only save the TargetIntrack wenn the particle first enters the target, not overwrite of backscattering happens
            {
                eventAction->EnteringEnergy = track->GetTotalEnergy();
                eventAction->TargetInTrack = preStepPoint->GetMomentumDirection();
            }
        }

        // Detect outgoing proton ----------------------------------------------------------------------------------------------------
        if (PostStepStatus == fGeomBoundary && PreStepVolume == "physTarget" && track->GetParticleDefinition() == ParticleDefinition)
        {
            eventAction->bIsExited = true;
            if (eventAction->TargetOutTrack == G4ThreeVector(0, 0, 0)) // Only save the Outtrack wenn the particle first exits the target, not overwrite of backscattering happens
            {
                // G4cout << "Primary particle left target" << postStepPoint->GetPosition() << G4endl;
                eventAction->TargetOutTrack = postStepPoint->GetMomentumDirection();
            }
        }

        // Track inelastic interaction -------------------------------------------------------------------------
        if (postStepPoint->GetProcessDefinedStep())
        {
            G4String ProcessName = postStepPoint->GetProcessDefinedStep()->GetProcessName();
            if (ProcessName == InelasitcProcessName && track->GetParticleDefinition() == ParticleDefinition && ParticleOriginVolume == "physWorld")
            {
                eventAction->bIsInelastic = true;
            }
        }

        // Track elastic interactions (hadronic elastic and coulomb) CoulombScat hadElastic
        if (postStepPoint->GetProcessDefinedStep())
        {
            G4String ProcessName = postStepPoint->GetProcessDefinedStep()->GetProcessName();
            if (track->GetParticleDefinition() == ParticleDefinition && (ProcessName == "CoulombScat" || ProcessName == "hadElastic") && ParticleOriginVolume == "physWorld")
            {
                eventAction->bIsElastic = true;
            }
        }
    }

    // Scintillator
    else if (CopyNo == 102)
    {
        if (PreStepStatus == fGeomBoundary && track->GetParticleDefinition() == ParticleDefinition && ParticleOriginVolume == "physWorld")
        {
            eventAction->bIsTrigger = true;
        }
    }

    // ALPIDEs
    else if (0 <= CopyNo && CopyNo <= 43)
    {
        // Monte Carlo: Consider only the primary particles to study the "real" scenario
        //  Three needed properties to be counted as passed
        //(i) Step at Boundary (to not double count for multiple steps)
        //(ii) particle is of specified (produced) type
        //(iii) particle produced in world (reject secondary particles)

        // Check beam profile
        if (PreStepStatus == fGeomBoundary && track->GetParticleDefinition() == ParticleDefinition && ParticleOriginVolume == "physWorld")
        {
            if (CopyNo == 0)
            {
                // Beam Profile
                eventAction->BeamPosDet0X = track->GetPosition().getX();
                eventAction->BeamPosDet0Y = track->GetPosition().getY();
                eventAction->BeamEnergy = track->GetKineticEnergy();
                eventAction->IncomingTrack = track->GetMomentumDirection();
            }
        }

        // Handle measurement: All Charged particles are measured (for classification -> neglect e-)
        G4double charge = track->GetParticleDefinition()->GetPDGCharge();
        if (PreStepStatus == fGeomBoundary && (charge == 1 || charge == -1) && !(track->GetParticleDefinition()->GetParticleName() == "e-" && track->GetKineticEnergy() < 1 * MeV))
        {

            // Hit in single ALPIDEs
            if (CopyNo < 10)
            {
                if (!(ContainsTrackID(*(eventAction->detector_hitvector_map[CopyNo]), hit.GetTrackID())))
                {
                    eventAction->detector_hitvector_map[CopyNo]->push_back(hit);
                }
            }
        }

        // Handle measurement: All Charged particles are measured for corryvreckan
        if (PreStepStatus == fGeomBoundary && (charge == 1 || charge == -1))
        {
            const SimulationConstruction *construction = static_cast<const SimulationConstruction *>(runManager->GetUserDetectorConstruction());
            double x_pitch = double(construction->x_pitch);
            double y_pitch = double(construction->y_pitch);
            double n_pixels = double(construction->n_pixels);
            double m_pixels = double(construction->m_pixels);

            if (0 <= CopyNo && CopyNo <= 5)
            {
                // Transform global to local coordinates
                G4TouchableHandle theTouchable = preStepPoint->GetTouchableHandle();
                G4ThreeVector worldPosition = hit.GetHitPosition();
                if (0 <= CopyNo && CopyNo <= 2) // ROI only for upstream
                {
                    if (abs(worldPosition.getX()) < 11.3 * mm && abs(worldPosition.getY()) < 3.2 * mm)
                    {
                        G4ThreeVector localPosition = theTouchable->GetHistory()->GetTopTransform().TransformPoint(worldPosition); // this still has (0,0) in the middle of the sensor

                        int row = static_cast<int>(floor((localPosition.getY() / y_pitch + static_cast<double>(m_pixels - 1) / 2.) + 0.5));
                        int column = static_cast<int>(floor((-localPosition.getX() / x_pitch + static_cast<double>(n_pixels - 1) / 2.) + 0.5));

                        int trigger_id = runManager->GetCurrentEvent()->GetEventID();
                        corryvreckan::Pixel *aPixel = new corryvreckan::Pixel("ALPIDE_" + std::to_string(CopyNo), column, row, 1., 1., trigger_id);

                        runAction->pixels_[CopyNo]->push_back(aPixel);
                    }
                }
                else
                {
                    G4ThreeVector localPosition = theTouchable->GetHistory()->GetTopTransform().TransformPoint(worldPosition); // this still has (0,0) in the middle of the sensor

                    int row = static_cast<int>(floor((localPosition.getY() / y_pitch + static_cast<double>(m_pixels - 1) / 2.) + 0.5));
                    int column = static_cast<int>(floor((-localPosition.getX() / x_pitch + static_cast<double>(n_pixels - 1) / 2.) + 0.5));

                    int trigger_id = runManager->GetCurrentEvent()->GetEventID();
                    corryvreckan::Pixel *aPixel = new corryvreckan::Pixel("ALPIDE_" + std::to_string(CopyNo), column, row, 1., 1., trigger_id);

                    runAction->pixels_[CopyNo]->push_back(aPixel);
                }
            }
        }
    }
    return true;
}

// Function to check if a trackID is already stored during the same event -> prevent multiple hits of same particle on one plane (delta electrons)
bool SensitiveDetector::ContainsTrackID(std::vector<UserHit> vec, int trackID)
{
    for (auto it = vec.begin(); it != vec.end(); it++)
    {
        if ((*it).GetTrackID() == trackID)
        {
            return true;
        }
    }
    return false;
}
