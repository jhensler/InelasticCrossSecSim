#include "RunAction.hh"

RunAction::RunAction()
{
    G4AnalysisManager *man = G4AnalysisManager::Instance();
    G4RunManager::GetRunManager()->SetPrintProgress(1);

    fMessenger = new G4GenericMessenger(this, "/runaction/", "Run Action");
    fMessenger->DeclareProperty("outputPath", OutputPath, "Path where rootfiles are stored");
    // Initial Values
    OutputPath = "../output/";

    // Create Histogramm to store counted particles (Monte Carlo) (ID 0)
    man->CreateH1("Pcount", "Particles counted", 7, 0, 7);

    // Create Histogramm to store classified events (Measurement) (ID 1)
    man->CreateH1("PcountMeas", "Particles counted (measurement)", 14, 0, 14);
    man->SetH1XAxisTitle(1, ";Trigger;Intrack;Outtrack;TITO;TITO.ElasIn;TITO.InelasTO;TINO;TINO.InelasTN;TINO.ElasOut;TINO.Bg;TIMO;TIMO.InelasTM;TIMO.Bg;TINO.Absorb");

    // Create Histogramm to store the incoming track angles in X (ID 2)
    man->CreateH1("IntrackAngleX", "Incoming track angle X", 100, -20, 20);
    man->SetH1XAxisTitle(2, "angle X [mrad]");

    // Create Histogramm to store the incoming track angles in Y (ID 3)
    man->CreateH1("IntrackAngleY", "Incoming track angle Y", 100, -20, 20);
    man->SetH1XAxisTitle(3, "angle Y [mrad]");

    // Create Histogramm to store the beam energy at ALP0 (ID 4)
    man->CreateH1("BeamEnergy", "BeamEnergy", 100, 0, 221);
    man->SetH1XAxisTitle(2, "kin. beam energy [MeV]");

    // Create 2D histogramm for a hitmap of the first detector layer (ID 0)
    man->CreateH2("hitmap", "ALPIDE_0 hitmap", 1024, -1024 / 2 * 29.24 * micrometer, 1024 * 29.24 / 2 * micrometer,
                  512, -512 * 26.88 / 2 * micrometer, 512 * 26.88 / 2 * micrometer);
}

RunAction::~RunAction()
{
    delete TimeStampTimer;
    delete event_tree_;
    delete pixel_tree_;
}

void RunAction::BeginOfRunAction(const G4Run *aRun)
{
    G4AnalysisManager *man = G4AnalysisManager::Instance();

    TimeStampTimer = new G4Timer();

    G4int runNumber = aRun->GetRunID();
    std::stringstream RunNameClass;
    RunNameClass << OutputPath << "run" << runNumber << "_class.root";
    man->OpenFile(RunNameClass.str());

    // Corry Export
    std::stringstream RunNameCorry;
    RunNameCorry << OutputPath << "run" << runNumber << "_corry.root";
    CorryFile = TFile::Open(RunNameCorry.str().c_str(), "RECREATE"); // Create output file

    CorryFile->cd();

    event_tree_ = new TTree("Event", (std::string("Tree of Events").c_str())); // Create event tree
    event_tree_->Bronch("global", "corryvreckan::Event", &event_);             // Create event branch w/ buffer event

    pixel_tree_ = new TTree("Pixel", (std::string("Tree of Pixels").c_str())); // Create pixel tree

    for (int DetID = 0; DetID <= 5; DetID++) // Loop over detectors
    {
        // Create branch for each detector
        std::string branch_name = "ALPIDE_" + std::to_string(DetID);
        pixel_tree_->Bronch(branch_name.c_str(), "std::vector<corryvreckan::Pixel*>", &(pixels_[DetID]));
    }

    TimeStampTimer->Start(); // Start timer for timestamps
}

void RunAction::EndOfRunAction(const G4Run *aRun)
{
    G4AnalysisManager *man = G4AnalysisManager::Instance();

    man->Write();
    man->CloseFile();

    CorryFile->Write();
    CorryFile->Close();
}
