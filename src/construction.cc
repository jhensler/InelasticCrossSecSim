#include "construction.hh"
#include "SensitiveDet.hh"

SimulationConstruction::SimulationConstruction()
{
    // Define UserMessenges
    fMessenger = new G4GenericMessenger(this, "/construction/", "Simulation Construction");
    fMessenger->DeclarePropertyWithUnit("target_thickness", "mm", target_thickness, "Thickness of the target");
    fMessenger->DeclareProperty("bIsTargetIn", bIsTargetIn, "Include / exclude target");

    // Initial Values
    target_thickness = 5 * mm;
    bIsTargetIn = true;

    // Once define the materials
    DefineMaterials();

    // Once define rotations
    DefineRotations();
}

SimulationConstruction::~SimulationConstruction()
{
    delete ALP0Rot;
    delete ALP1Rot;
    delete ALP2Rot;
    delete ALP3Rot;
    delete ALP4Rot;
    delete ALP5Rot;
}

G4VPhysicalVolume *SimulationConstruction::Construct()
{
    // SOLID VOLUMES ......oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........
    solidWorld = new G4Box("solidWorld", world_halfsizeX, world_halfsizeY, world_halfsizeZ); // half of the length
    solidTarget = new G4Box("solidTarget", target_halfsizeX, target_halfsizeY, target_thickness / 2);
    solidDet = new G4Box("solidDet", ALPIDE_halfsizeX, ALPIDE_halfsizeY, ALPIDE_halfsizeZ);
    solidSci = new G4Box("solidSci", sci_halfsizeX, sci_halfsizeY, sci_halfsizeZ);
    solidSciCover = new G4Tubs("solidSciCover", 15.25 * mm, 16.25 * mm, 30 * mm, 0., 2 * M_PI * rad);

    solidouterTholder = new G4Box("solidouterTholder", 9.9 / 2 * cm, 9. / 2 * cm, 1. / 2 * cm);
    solidinnerTholder = new G4Box("solidinnerTholder", innerTholder_halfsizeX, innerTholder_halfsizeY, innerTholder_halfsizeZ);
    solidwindowTholder = new G4Box("solidwindowTholder", windowTholder_halfsizeX, windowTholder_halfsizeY, windowTholder_halfsizeZ);
    solidpreTholder = new G4SubtractionSolid("solidpreTholder", solidouterTholder, solidinnerTholder, 0, G4ThreeVector(0., 0.45 * cm, 0.));
    solidTholder = new G4SubtractionSolid("solidTholder", solidpreTholder, solidwindowTholder, 0, G4ThreeVector(0., 0.5 * cm, 0.));

    // LOGIC VOLUMES ......oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........
    logicWorld = new G4LogicalVolume(solidWorld, worldMat, "logicWorld");
    logicTarget = new G4LogicalVolume(solidTarget, targetMat, "logicTarget");
    logicALPIDE = new G4LogicalVolume(solidDet, detMat, "logicALPIDE");
    logicSci = new G4LogicalVolume(solidSci, sciMat, "logicSci");
    logicSciCover = new G4LogicalVolume(solidSciCover, targetMat, "logicSciCover");

    logicTholder = new G4LogicalVolume(solidTholder, TholderMat, "logicTholder");

    // PHYSIC VOLUMES ......oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........
    physWorld = new G4PVPlacement(0, G4ThreeVector(0., 0., 0.), logicWorld, "physWorld", 0, false, 0, true);
    physSci = new G4PVPlacement(0, G4ThreeVector(0., 0., sciZpos), logicSci, "physSci", logicWorld, false, 102, true);

    // Place target and holder
    if (bIsTargetIn)
    {
        physTarget = new G4PVPlacement(0, G4ThreeVector(0, 0., 0), logicTarget, "physTarget", logicWorld, false, 101, true);
    }
    physTholder = new G4PVPlacement(0, G4ThreeVector(0., -0.15 * cm, 0.), logicTholder, "physTholder", logicWorld, false, 103, true);

    // Single ALPIDEs

    physALPIDE0 = new G4PVPlacement(ALP0Rot, ALP0Loc, logicALPIDE, "physALPIDE0", logicWorld, false, 0, true);
    physALPIDE1 = new G4PVPlacement(ALP1Rot, ALP1Loc, logicALPIDE, "physALPIDE1", logicWorld, false, 1, true);
    physALPIDE2 = new G4PVPlacement(ALP2Rot, ALP2Loc, logicALPIDE, "physALPIDE2", logicWorld, false, 2, true);
    physALPIDE3 = new G4PVPlacement(ALP3Rot, ALP3Loc, logicALPIDE, "physALPIDE3", logicWorld, false, 3, true);
    physALPIDE4 = new G4PVPlacement(ALP4Rot, ALP4Loc, logicALPIDE, "physALPIDE4", logicWorld, false, 4, true);
    physALPIDE5 = new G4PVPlacement(ALP5Rot, ALP5Loc, logicALPIDE, "physALPIDE5", logicWorld, false, 5, true);

    physSciCover = new G4PVPlacement(SciCoverRot, G4ThreeVector(-sci_halfsizeX - 30 * mm, 0, sciZpos), logicSciCover, "physSciCover", logicWorld, false, 103, true);

    return physWorld;
}

void SimulationConstruction::ConstructSDandField()
{

    SensitiveDetector *SensTarget = new SensitiveDetector("SensitiveTarget");
    logicTarget->SetSensitiveDetector(SensTarget);

    SensitiveDetector *SensALPIDE = new SensitiveDetector("SensitiveALPIDE");
    logicALPIDE->SetSensitiveDetector(SensALPIDE);

    SensitiveDetector *SensSci = new SensitiveDetector("SensitiveScintillator");
    logicSci->SetSensitiveDetector(SensSci);
}

void SimulationConstruction::DefineMaterials()
{
    // MATERIALS ......oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........
    G4NistManager *nist = G4NistManager::Instance();
    worldMat = nist->FindOrBuildMaterial("G4_AIR");
    targetMat = nist->FindOrBuildMaterial("G4_Al");
    detMat = nist->FindOrBuildMaterial("G4_Si");
    sciMat = nist->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
    TholderMat = nist->FindOrBuildMaterial("G4_POLYACRYLONITRILE");
}

void SimulationConstruction::DefineRotations()
{
    ALP0Rot = new G4RotationMatrix();
    ALP1Rot = new G4RotationMatrix();
    ALP2Rot = new G4RotationMatrix();
    ALP3Rot = new G4RotationMatrix();
    ALP4Rot = new G4RotationMatrix();
    ALP5Rot = new G4RotationMatrix();
    SciCoverRot = new G4RotationMatrix();

    // Apply rotations:
    ALP0Rot->rotateX(ALP0rotX);
    ALP0Rot->rotateY(ALP0rotY);
    ALP0Rot->rotateZ(ALP0rotZ);

    ALP1Rot->rotateX(ALP1rotX);
    ALP1Rot->rotateY(ALP1rotY);
    ALP1Rot->rotateZ(ALP1rotZ);

    ALP2Rot->rotateX(ALP2rotX);
    ALP2Rot->rotateY(ALP2rotY);
    ALP2Rot->rotateZ(ALP2rotZ);

    ALP3Rot->rotateX(ALP3rotX);
    ALP3Rot->rotateY(ALP3rotY);
    ALP3Rot->rotateZ(ALP3rotZ);

    ALP4Rot->rotateX(ALP4rotX);
    ALP4Rot->rotateY(ALP4rotY);
    ALP4Rot->rotateZ(ALP4rotZ);

    ALP5Rot->rotateX(ALP5rotX);
    ALP5Rot->rotateY(ALP5rotY);
    ALP5Rot->rotateZ(ALP5rotZ);

    SciCoverRot->rotateY(90 * deg);
}
