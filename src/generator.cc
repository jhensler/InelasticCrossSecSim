#include "generator.hh"

PrimaryGenerator::PrimaryGenerator()
{
    fGenParSource = new G4GeneralParticleSource(); // Construct a GeneralParticleSource (already includes one SingleParticleSource)

    // Define particle
    G4ParticleTable *particleTable = G4ParticleTable::GetParticleTable();
    G4String particleName = "proton";
    G4ParticleDefinition *particle = particleTable->FindParticle(particleName);
    fGenParSource->GetCurrentSource()->SetParticleDefinition(particle);

    // Define particles momentum direction
    G4ThreeVector momentum(0., 0., 1.);
    fGenParSource->GetCurrentSource()->GetAngDist()->SetParticleMomentumDirection(momentum);

    // Define particle energy distribution and default value
    fGenParSource->GetCurrentSource()->GetEneDist()->SetEnergyDisType("Mono");
    fGenParSource->GetCurrentSource()->GetEneDist()->SetMonoEnergy(221 * MeV);

    // Defina beam position distribution
    G4double FWHM = 6.59 * mm;
    G4double sigma = (1 / 2.35482004503) * FWHM;

    fGenParSource->GetCurrentSource()->GetPosDist()->SetPosDisType("Beam");  // Select a gaussian pos. distr.
    fGenParSource->GetCurrentSource()->GetPosDist()->SetBeamSigmaInR(sigma); // Select corresponding sigma of the pos. distr.

    // Define beam angular distribution
    fGenParSource->GetCurrentSource()->GetAngDist()->SetAngDistType("beam2d");                             // Select a 2d gaussian ang. distr.
    fGenParSource->GetCurrentSource()->GetAngDist()->SetBeamSigmaInAngX(5.3e-3 * rad);                     // Select sigma in x for ang. distr.
    fGenParSource->GetCurrentSource()->GetAngDist()->SetBeamSigmaInAngY(4.3e-3 * rad);                     // Select sigma in y for ang. distr.
    fGenParSource->GetCurrentSource()->GetAngDist()->DefineAngRefAxes("angref1", G4ThreeVector(-1, 0, 0)); // Rotate GPS to point along z-axis
    fGenParSource->GetCurrentSource()->GetAngDist()->DefineAngRefAxes("angref2", G4ThreeVector(0, 1, 0));  // Rotate GPS to point along z-axis

    // Define beam center (starting point)
    fGenParSource->GetCurrentSource()->GetPosDist()->SetCentreCoords(G4ThreeVector(5.06 * mm, 0., -d_BeamStart_target));
}

PrimaryGenerator::~PrimaryGenerator()
{
    delete fGenParSource;
}

void PrimaryGenerator::GeneratePrimaries(G4Event *anEvent)
{
    fGenParSource->GetCurrentSource()->GeneratePrimaryVertex(anEvent);
}
