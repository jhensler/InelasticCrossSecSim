#include "EventAction.hh"

EventAction::EventAction()
{
    fMessenger = new G4GenericMessenger(this, "/eventaction/", "Event Action");
    fMessenger->DeclareProperty("RestrictInTracks", bIsRestrictedInTrack, "Should the incoming tracks be restricted by area?");
    fMessenger->DeclarePropertyWithUnit("SetResX", "mm", hResX, "Half-height of restriction in x");
    fMessenger->DeclarePropertyWithUnit("SetResY", "mm", hResY, "Half-height of restriction in y");

    // Initial Values
    bIsRestrictedInTrack = false;
    hResX = 3.4 * mm; // for 5mm
    hResY = 1.4 * mm; // for 5mm

    Exception = new G4ExceptionHandler;
}

EventAction::~EventAction()
{
    delete fMessenger;
    delete Exception;
}

void EventAction::BeginOfEventAction(const G4Event *event)
{
    // Get RunManager and runAction
    runManager = static_cast<RunManager *>(RunManager::GetRunManager());
    if (runManager && runManager->runAction)
    {
        runAction = runManager->runAction;
    }
    else
    {
        Exception->Notify("EventAction", "No runAction found", FatalException, "No runAction could by found by the EventAction!");
    }

    // Get AnalysisManager
    G4AnalysisManager *man = G4AnalysisManager::Instance();

    // Run status
    G4int eventID = event->GetEventID();
    G4Run *run = static_cast<G4Run *>(G4RunManager::GetRunManager()->GetNonConstCurrentRun());
    G4int nOfEvents = run->GetNumberOfEventToBeProcessed();
    G4double perCent = 10.; // status increment in percent

    if (fmod(eventID, double(nOfEvents * perCent * 0.01)) == 0)
    {
        time_t my_time = time(NULL);
        tm *ltm = localtime(&my_time);
        G4double status = (100 * (eventID / double(nOfEvents)));
        std::cout << "=> Event " << eventID << " starts (" << status << "%, " << ltm->tm_hour << ":" << ltm->tm_min << ":" << ltm->tm_sec << ")" << std::endl;
    }

    // Reset count bools and other data storage objects
    StartEventTime = -1;
    EndEventTime = -1;

    bIsEntered = false;
    bIsExited = false;
    bIsPassed = false;
    bIsInelastic = false;
    bIsElastic = false;

    BeamPosDet0X = 0;
    BeamPosDet0Y = 0;
    BeamEnergy = 0;

    bIsTrigger = false;
    bIsInTrack = false;
    NrInTrack = 0;
    NrOutTrack = 0;
    bIsOutSingleTrack = false;
    bIsOutMultipleTrack = false;
    bIsNoOutTrack = false;

    HitsDet0.clear();
    HitsDet1.clear();
    HitsDet2.clear();
    HitsDet3.clear();
    HitsDet4.clear();
    HitsDet5.clear();

    runAction->pixels_[0]->clear();
    runAction->pixels_[1]->clear();
    runAction->pixels_[2]->clear();
    runAction->pixels_[3]->clear();
    runAction->pixels_[4]->clear();
    runAction->pixels_[5]->clear();

    // Reset tracks
    TargetInTrack = G4ThreeVector(0, 0, 0);
    TargetOutTrack = G4ThreeVector(0, 0, 0);
    IncomingTrack = G4ThreeVector(0, 0, 0);
    EnteringEnergy = 0 * MeV;

    // Get Start Event Timestamp
    runAction->TimeStampTimer->Stop();
    // StartEventTime = runManager->GetCurrentEvent()->GetEventID();
    StartEventTime = runAction->TimeStampTimer->GetRealElapsed() * 1e6;

    // G4cout << "Start: " << StartEventTime << G4endl;
}
void EventAction::EndOfEventAction(const G4Event *event)
{
    G4AnalysisManager *man = G4AnalysisManager::Instance(); // Get AnalysisManager

    // Get End Event Timestamp
    runAction->TimeStampTimer->Stop();
    // EndEventTime = runManager->GetCurrentEvent()->GetEventID();
    EndEventTime = runAction->TimeStampTimer->GetRealElapsed() * 1e6;

    // G4cout << "End: " << EndEventTime << G4endl;

    // Inelastic cross section data storage (truth) --------------------------------------------------------------------------------------------------------------------
    // Store number of particles entering the target
    if (bIsEntered)
    {
        man->FillH1(0, 0);
    }
    if (bIsInelastic)
    {
        man->FillH1(0, 3);
    }

    // Handle measurement signatures: All Charged particles are measured -------------------------------------------------------------------------------------------------
    // Distinguish three measurement signatures: i) track in track out (TITO), ii)track in no (track) out (TINO), iii) track in, multiple out (TIMO)
    // Track in: 3 planes infront hit + scintillator, Track out: the first plane after target is hit and min 3 planes total after target

    // Determine the number of triggers
    if (bIsTrigger)
    {
        man->FillH1(1, 0);
    }

    // Extract all "in" tracks (in general track through all first three planes, so also charged secondaries)
    std::unordered_map<int, int> freq_in; // Store the frequency of each TrackID in the first three planes
    for (int CopyNo = 0; CopyNo < 3; CopyNo++)
    {
        for (int i = 0; i < detector_hitvector_map[CopyNo]->size(); i++)
        {
            if (bIsRestrictedInTrack)
            {
                // Only register these hits if in specified area on ALPIDEs
                G4ThreeVector HitPosition = (*(detector_hitvector_map[CopyNo]))[i].GetHitPosition();
                if (abs(HitPosition.getX()) < hResX && abs(HitPosition.getY()) < hResY)
                {
                    freq_in[(*(detector_hitvector_map[CopyNo]))[i].GetTrackID()]++;
                }
            }
            else
            {
                freq_in[(*(detector_hitvector_map[CopyNo]))[i].GetTrackID()]++;
            }
        }
    }
    for (auto it = freq_in.begin(); it != freq_in.end(); it++)
    {
        // Definition of incoming tracks: Hit all three planes + scintillator
        if (it->second >= 3 && bIsTrigger)
        {
            bIsInTrack = true;
            NrInTrack++;
        }
    }

    if (bIsInTrack)
    {
        man->FillH1(1, 1);
    }

    // Extract all out tracks (differentiate between one out track and multiple out tracks)
    std::unordered_map<int, int> freq_out;          // Store the frequency of each TrackID in the 5 planes after the target
    std::unordered_map<int, bool> bHit3rdPlane_map; //  Store if the first plane after target was hit for each TrackID

    // Count frequecy of TrackIDs in single ALPIDEs after the target
    for (int CopyNo = 3; CopyNo < 6; CopyNo++)
    {
        for (int i = 0; i < (detector_hitvector_map[CopyNo])->size(); i++)
        {
            G4int TrackID = (*(detector_hitvector_map[CopyNo]))[i].GetTrackID();
            freq_out[TrackID]++; // Add one for that TrackID

            if (CopyNo == 3)
            {
                bHit3rdPlane_map[(*(detector_hitvector_map[CopyNo]))[i].GetTrackID()] = true; // Mark this TrackID as hit in the 3rd plane
            }
        }
    }

    for (auto it = freq_out.begin(); it != freq_out.end(); it++)
    {
        // OutTrack requirement: 3rd plane hit and total min 3 planes hit
        if (bHit3rdPlane_map[it->first] && it->second >= 3)
        {
            NrOutTrack++;
        }
    }

    // Fill number of out tracks
    for (int i = 0; i < NrOutTrack; i++)
    {
        if (bIsInTrack) // Only register out-tracks if intrack was registered, avoid no track in - track out events
            man->FillH1(1, 2);
    }

    if (NrOutTrack == 1)
    {
        bIsOutSingleTrack = true;
    }
    else if (NrOutTrack > 1)
    {
        bIsOutMultipleTrack = true;
    }
    else if (NrOutTrack == 0)
    {
        bIsNoOutTrack = true;
    }

    // Categorise proccesses ------------------------------------------------------------------------------
    // (I)TITO
    if (bIsInTrack && bIsOutSingleTrack)
    {
        man->FillH1(1, 3);

        // Two possible cases considerd:
        //(I.i) TITO.ElasIn (Elastic interacted primary particle)
        if (!bIsInelastic)
        {
            man->FillH1(1, 4);
        }

        // (I.ii) TITO.InelasTO (Inelastic interaction in target with one charged particle in acceptance)
        else if (bIsInelastic)
        {
            man->FillH1(1, 5);
        }
    }
    //(II) TINO
    // Calculate MSC angle to determine ElasOut cut
    G4RunManager *runManager = (G4RunManager::GetRunManager());
    if (!runManager)
        return;

    const SimulationConstruction *construction = static_cast<const SimulationConstruction *>(runManager->GetUserDetectorConstruction());

    // For proton z = 1 and m = 938 MeV, and for Al X_0 = 8.897 cm
    G4double msc_cut = 3 * CalculateMSCAngle(EnteringEnergy, 1, 8.897 * cm, construction->target_thickness, 938 * MeV);

    G4double theta, phi;
    std::tie(theta, phi) = CalculateTotalScattering(TargetInTrack, TargetOutTrack);

    if (bIsInTrack && bIsNoOutTrack)
    {
        man->FillH1(1, 6);

        // Three possible cases considered
        G4bool bElasticCondition = !bIsInelastic && (bIsElastic || abs(theta) > msc_cut || abs(phi) > msc_cut);
        //(II.i) TINO.InelasTN (Inelastic interaction with no charged particle in acceptance)
        if (bIsInelastic)
        {
            man->FillH1(1, 7);
        }

        //(II.ii) TINO.ElasOut (Single elastic scattering in target out of acceptance)
        else if (bElasticCondition)
        {
            man->FillH1(1, 8);
        }
        //(II.iii) TINO.Bg (background, like scattering in ALPIDEs / divergence of beam / inelastic in ALPIDE)
        else
        {
            man->FillH1(1, 9);

            // // Debug:
            // //     Display one current event
            // G4UImanager *uiManager = G4UImanager::GetUIpointer();
            // uiManager->ApplyCommand("/vis/enable");
            // G4EventManager *eventManager = G4EventManager::GetEventManager();
            // eventManager->KeepTheCurrentEvent();
            // G4RunManager::GetRunManager()->AbortRun();
        }

        //(II.iv) TINO.Absorb(primary particl enetered the target but did not exit. No inelastic interaction and no elastic interaction occured.)
        if (bIsEntered && !bIsExited && !bIsInelastic && !bElasticCondition)
        {
            man->FillH1(1, 13);
        }
    }

    //(III) TIMO
    if (bIsInTrack && bIsOutMultipleTrack)
    {
        man->FillH1(1, 10);

        // (III.i) TIMO.InelasTM (inelastic interaction in target with multiple charged secondaries in acceptance)
        if (bIsInelastic)
        {
            man->FillH1(1, 11);
        }
        // (III.ii) TIMO.Bg (i.e. delta electrons, whill be filtered out by tracking)
        if (!bIsInelastic)
        {
            man->FillH1(1, 12);
        }
    }

    // Storage of beam related data  ------------------------------------------------------------------------------------------------------------
    if (BeamPosDet0X != 0 && BeamPosDet0Y != 0)
    {
        // Store beam energy at ALP0
        man->FillH1(4, BeamEnergy);

        // Store beam hitmap on ALP0
        if (bIsTrigger)
        {
            man->FillH2(0, -BeamPosDet0X, BeamPosDet0Y); // - needed to transform to local ALPIDE coordinates (x axis inverted in simulation)
        }

        // Calculate incoming track angle
        G4ThreeVector AxisZ = G4ThreeVector(0., 0., 1.);
        G4ThreeVector IncomingTrackX = G4ThreeVector(IncomingTrack.getX(), 0., IncomingTrack.getZ());
        G4ThreeVector IncomingTrackY = G4ThreeVector(0., IncomingTrack.getY(), IncomingTrack.getZ());
        G4double IncomingAngleX = IncomingTrackX.angle(AxisZ) * 1e3;
        G4double IncomingAngleY = IncomingTrackY.angle(AxisZ) * 1e3;

        // Acount for sign of the angle (counter clockwise = positive)
        if (IncomingTrackX.getX() > AxisZ.getX())
        {
            IncomingAngleX = -IncomingAngleX;
        }
        if (IncomingTrackY.getY() < AxisZ.getY())
        {
            IncomingAngleY = -IncomingAngleY;
        }

        man->FillH1(2, IncomingAngleX);
        man->FillH1(3, IncomingAngleY);
    }

    // Data storage for corry
    CorryExport();
}

void EventAction::CorryExport()
{

    if (runAction->CorryFile)
    {

        // Create and save event
        uint32_t trigger_id = uint32_t(runManager->GetCurrentEvent()->GetEventID());
        std::map<uint32_t, double> trigger_list;
        trigger_list.insert(std::make_pair(trigger_id, StartEventTime));

        corryvreckan::Event *new_event = new corryvreckan::Event(0., 0., trigger_list); // Create Corry Event

        runAction->event_ = new_event;  // Point buffer event to newly created event
        runAction->event_tree_->Fill(); // Fill evetn in tree

        delete new_event;

        runAction->pixel_tree_->Fill();
    }
    else
    {
        Exception->Notify("EventAction", "No CorryFile found", FatalException, "No runAction->CorryFile could by found by the EventAction!");
    }
}

// Calculates the mean multiple scattering angle. Input energy in MeV
G4double EventAction::CalculateMSCAngle(G4double E, int z, G4double X_0, G4double x, G4double m)
{
    G4double p = sqrt(pow(E, 2) - pow(m, 2));
    G4double beta = p / E;

    return 13.6 * MeV / (beta * p) * z * sqrt(x / X_0) * (1 + 0.038 * log(x * pow(z, 2) / (X_0 * pow(beta, 2))));
}

std::pair<G4double, G4double> EventAction::CalculateTotalScattering(G4ThreeVector TargetIntrack, G4ThreeVector TargetOutTrack)
{
    G4double phi = 0;
    G4double theta = 0;
    if (TargetOutTrack != G4ThreeVector(0, 0, 0))
    {
        // Calculate scattering angle in phi and theta (spherical coordinates, phi from (-pi, pi) and theta from (-pi/2, pi/2)
        // where phi, theta = (0,0 is the forwards unaffected direction))
        // G4cout << "TargetIntrack: " << TargetIntrack << " TargetOuttrack: " << TargetOuttrack << G4endl;
        // To calculate phi, project incoming track and outgoing track in XZ plane
        G4ThreeVector TrackBeforeX = G4ThreeVector(TargetInTrack.getX(), 0, TargetInTrack.getZ());
        G4ThreeVector TrackAfterX = G4ThreeVector(TargetOutTrack.getX(), 0, TargetOutTrack.getZ());
        phi = TrackBeforeX.angle(TrackAfterX);

        // To calculate theta, the angle between the outoing track in XZ plane and the outoing track needs to be considered
        //  Note: The incoming particle is lie in the XZ plane. To correct, maybe substract the theta of incoming track.
        G4ThreeVector TrackBeforeY = G4ThreeVector(0, TargetInTrack.getY(), TargetInTrack.getZ());
        G4ThreeVector TrackAfterY = G4ThreeVector(0, TargetOutTrack.getY(), TargetOutTrack.getZ());
        theta = TrackBeforeY.angle(TrackAfterY);

        // Acount for sign of the angle (counter clockwise = positive)
        if (TrackBeforeX.getX() > TrackAfterX.getX())
        {
            phi = -phi;
        }
        if (TrackBeforeY.getY() > TrackAfterY.getY())
        {
            theta = -theta;
        }
        return std::make_pair(phi, theta);
    }
    return std::make_pair(0, 0);
}