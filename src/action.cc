#include "action.hh"

ActionInitialization::ActionInitialization()
{
    generator = new PrimaryGenerator();
    eventAction = new EventAction();
    runAction = new RunAction();
}

ActionInitialization::~ActionInitialization()
{
    delete generator;
    delete eventAction;
    delete runAction;
}

void ActionInitialization::Build() const
{
    SetUserAction(generator);
    SetUserAction(eventAction);
    SetUserAction(runAction);

    static_cast<RunManager *>(RunManager::GetRunManager())->eventAction = eventAction;
    static_cast<RunManager *>(RunManager::GetRunManager())->runAction = runAction;
}
